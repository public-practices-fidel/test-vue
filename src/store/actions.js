import axios from "axios"

const BASE_URL = "https://jsonplaceholder.typicode.com/posts/"

//posts
export const getPosts = ({ commit }) => {
    axios.get(BASE_URL)
        .then(res => {
            commit('SET_POSTS', res.data)
        })
}

//  single post
export const getPost = ({ commit }, postId) => {
    axios.get(BASE_URL + postId)
        .then(res => {
            commit('SET_POST', res.data)
        })
}

