// posts
export const SET_POSTS = (state, posts) => {
    state.posts = posts,
        state.fetchingPostsData = false
}

// single post
export const SET_POST = (state, post) => {
    state.post = post
}