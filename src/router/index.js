import Vue from "vue";
import Router from "vue-router";
import Home from '@/components/views/Home'
import About from '@/components/views/About'
import Contact from '@/components/views/Contact'
import PostDetail from '@/components/views/PostDetail'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            component: Home
        },
        {
            path: '/about',
            component: About
        },
        {
            path: '/contact',
            component: Contact
        },
        {
            path: '/post/:id',
            name: 'post',
            component: PostDetail,
            props: true
        },
    ]
})